FROM registry.gitlab.com/brianjaustin/docker-lazarus:fixes2.0

# Add cross-compile dependencies
ADD rpi/ /rpi/
RUN apt-get install -y binutils-arm-linux-gnueabihf

# Install FPC arm cross compiler
ADD settings.ini .
RUN fpcup --inifile=settings.ini --inisection=linuxarm \
          --crossbindir=/usr/bin --crosslibdir=/rpi/lib/arm-linux-gnueabihf

# Fix library paths to be what FPC expects
RUN cd /rpi/lib/arm-linux-gnueabihf && \
    ln -s libpthread.so.0 libpthread.so && \
    ln -s libgthread-2.0.so.0 libgthread-2.0.so && \
    ln -s libdl.so.2 libdl.so && \
    ln -s libgdk-x11-2.0.so.0 libgdk-x11-2.0.so && \
    ln -s libgdk_pixbuf-2.0.so.0 libgdk_pixbuf-2.0.so && \
    ln -s libgtk-x11-2.0.so.0 libgtk-x11-2.0.so && \
    ln -s libgobject-2.0.so.0 libgobject-2.0.so && \
    ln -s libglib-2.0.so.0 libglib-2.0.so && \
    ln -s libgmodule-2.0.so.0 libgmodule-2.0.so && \
    ln -s libpango-1.0.so.0 libpango-1.0.so && \
    ln -s libcairo.so.2 libcairo.so && \
    ln -s libatk-1.0.so.0 libatk-1.0.so && \
    ln -s libc.so.6 libc.so && \
    ln -sf ld-2.28.so ld-linux-armhf.so.3
# (the last lib exists but the simlink is broken)
